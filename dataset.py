import torch
from torch.utils import data
import os, sys
#import cPickle as pickle
import pickle
from PIL import Image
import torchvision.transforms as transforms
import numpy as np

class Dataset(data.Dataset):
  def __init__(self, split='train', transform=None, root='data/dogscats'):
    with open(os.path.join(root, split+'.pkl'), 'rb') as f:
      self.data = pickle.load(f)
    self.transform = transform
    self.root = root
    self.split = split

  def __len__(self):
    return len(self.data['images'])

  def __getitem__(self, index):
    img_name = self.data['images'][index]
    try:
      img_name = img_name.decode()
    except:
      pass
    X = Image.open(os.path.join(self.root, img_name)) #rgb image
    if self.transform is not None:
      X = self.transform(X)
    y = self.data['labels'][index]
    return X, y

def fetch_dataloaders(types, data_dir, params):
  data_transforms = {
    'train': transforms.Compose([
      transforms.RandomResizedCrop(params.input_size),
      transforms.RandomHorizontalFlip(),
      transforms.ToTensor(),
      transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
      ]),
    'val': transforms.Compose([
      transforms.Resize(params.train_resize), #256
      transforms.CenterCrop(params.input_size), #224
      transforms.ToTensor(),
      transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
      ]),
    }
  data_transforms['test'] = data_transforms['val']

  # This is training data but for performing validation on it
  data_transforms['train_val'] = data_transforms['val']

  dataloaders = {}
  for split in types:
    if split == 'train':
      shuffle = True
    else:
      shuffle = False
    dl = data.DataLoader(Dataset(split=split,
                                transform=data_transforms[split],
                                root=data_dir),
                         batch_size=params.batch_size,
                         shuffle=shuffle,
                         num_workers=params.num_workers,
                         pin_memory=params.cuda)
    dataloaders[split] = dl
  return dataloaders


