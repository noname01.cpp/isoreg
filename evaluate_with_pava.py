"""Evaluates the model"""

import argparse
import logging
import os

import numpy as np
import torch
from torch.autograd import Variable
import utils
import net
import dataset as data_loader
from pava import BCEPava
from tqdm import tqdm
from interpolate import Interp1d

parser = argparse.ArgumentParser()
parser.add_argument('--data_dir', default='data/dogscats', help="Directory containing the dataset")
parser.add_argument('--model_dir', default='experiments/base_model', help="Directory containing params.json")
parser.add_argument('--restore_file', default='best', help="name of the file in --model_dir \
                     containing weights to load")

def BCELoss(probs, labels, epsilon=1e-8):
  """
    probs: (N,) numpy array of probabilities
    targets: (N,) numpy array of labels which has either 0, or 1
  """
  probs = np.clip(probs, epsilon, 1. - epsilon)
  loss_sum = -np.log(probs[labels==1]).sum() - np.log(1-probs[labels==0]).sum()
  return loss_sum/float(probs.shape[0])

def run_isoreg(model, pava_dataloader, params, all_datapoints, epoch, summary_writer, pava_set_name, pava_scores=None, pava_targets=None):
  model.eval()
  all_scores, all_labels = [], []
  logging.info('Computing scores and labels for isoreg on {}'.format(pava_set_name))
  for data_batch, labels_batch in tqdm(pava_dataloader):
    # move to GPU if available
    if params.cuda:
        data_batch, labels_batch = data_batch.cuda(async=True), labels_batch.cuda(async=True)
    # fetch the next evaluation batch
    data_batch, labels_batch = Variable(data_batch), Variable(labels_batch)

    # compute model output
    output_batch, probs_batch = model(data_batch)
    scores_batch = output_batch
    if params.pava_after_sigmoid:
      scores_batch = probs_batch
    if pava_scores is not None:
      scores_batch = Interp1d().forward(pava_scores, pava_targets, scores_batch)
      scores_batch = torch.clamp(scores_batch, min=1e-8, max=1-1e-8)

    # extract data from torch Variable, move to cpu, convert to numpy arrays
    scores_batch = scores_batch.data.cpu().numpy()
    labels_batch = labels_batch.data.cpu().numpy()

    all_scores.append(scores_batch)
    all_labels.append(labels_batch)

  is_logit = not (params.pava_after_sigmoid or pava_scores is not None)
  isoreg = BCEPava(is_logit=is_logit, smooth=params.smooth, sigma=params.sigma)
  all_labels = np.hstack(all_labels) # (N,) array of labels
  all_scores = np.vstack(all_scores).reshape(all_labels.shape) #(N,) array of scores
  logging.info('Fitting the isotonic regression function ...')
  isoreg.fit(all_scores, all_labels)
  all_preds = isoreg.targets
  #all_preds = isoreg.predict(all_scores)
  all_datapoints[pava_set_name] = {'scores': all_scores, 'labels': all_labels,
                                   'preds': all_preds}
  summary_writer.add_histogram('isoreg/epoch/scores', all_scores, epoch)
  summary_writer.add_histogram('isoreg/epoch/preds', all_preds, epoch)
  logging.info('- Done -')
  return isoreg

def run_evaluate(model, loss_fn, dataloader, params, epoch, summary_writer, evaluation_metrics, metrics, set_name, old_pava_scores, old_pava_targets, pava_scores, pava_targets):
  model.eval()
  logging.info('Evaluation on {}'.format(set_name))
  summ = []
  total = len(dataloader)
  for i, (data_batch, labels_batch) in enumerate(tqdm(dataloader)):
    # move to GPU if available
    if params.cuda:
      data_batch, labels_batch = data_batch.cuda(async=True), labels_batch.cuda(async=True)
    iteration = total*epoch + i
    # fetch the next evaluation batch
    data_batch, labels_batch = Variable(data_batch), Variable(labels_batch)

    # compute model output
    output_batch, probs_batch = model(data_batch)
    scores_batch = output_batch
    threshold = 0.0
    if params.pava_after_sigmoid:
      scores_batch = probs_batch
    if old_pava_scores is None:
      loss = loss_fn(output_batch, labels_batch, is_logit=True)
    else:
      output_batch = Interp1d().forward(old_pava_scores, old_pava_targets, scores_batch)
      output_batch = torch.clamp(output_batch, min=1e-8, max=1-1e-8)
      loss = loss_fn(output_batch, labels_batch, is_logit=False)
      threshold = 0.5

    isoreg_scores_batch = Interp1d().forward(pava_scores, pava_targets, scores_batch)
    isoreg_scores_batch = torch.clamp(isoreg_scores_batch, min=1e-8, max=1-1e-8)
    isoreg_loss = loss_fn(isoreg_scores_batch, labels_batch, is_logit=False)


    # extract data from torch Variable, move to cpu, convert to numpy arrays
    output_batch = output_batch.data.cpu().numpy()
    labels_batch = labels_batch.data.cpu().numpy()
    probs_batch  = probs_batch.data.cpu().numpy()
    isoreg_scores_batch = isoreg_scores_batch.data.cpu().numpy()
    isoreg_loss = isoreg_loss.data.cpu().numpy()

    isoreg_accuracy = net.accuracy(isoreg_scores_batch, labels_batch, threshold=0.5)

    # compute all metrics on this batch
    #summary_batch = {metric: metrics[metric](output_batch, labels_batch)
    #                 for metric in metrics}
    summary_batch = {'acc_before_isoreg': net.accuracy(output_batch, labels_batch, threshold=threshold)}
    summary_batch['loss_before_isoreg'] = loss.data.cpu().numpy()
    summary_batch['acc_after_isoreg'] = isoreg_accuracy
    summary_batch['loss_after_isoreg'] = isoreg_loss
    summary_writer.add_scalar('eval/batch/acc_before_isoreg', summary_batch['acc_before_isoreg'], iteration)
    summary_writer.add_scalar('eval/batch/loss_before_isoreg', summary_batch['loss_before_isoreg'], iteration)
    summary_writer.add_scalar('eval/batch/acc_after_isoreg', summary_batch['acc_after_isoreg'], iteration)
    summary_writer.add_scalar('eval/batch/loss_after_isoreg', summary_batch['loss_after_isoreg'], iteration)
    summ.append(summary_batch)

  # compute mean of all metrics in summary
  metrics_mean = {metric:np.mean([x[metric] for x in summ]) for metric in summ[0]}
  for metric_name, metric_value in metrics_mean.items():
    summary_writer.add_scalar('eval/epoch/'+metric_name, metric_value, epoch)
  metrics_string = " ; ".join("{}: {:05.3f}".format(k, v) for k, v in metrics_mean.items())
  logging.info("- Eval metrics : " + metrics_string)
  evaluation_metrics['eval_on_'+set_name] = metrics_mean
  return metrics_mean


def evaluate_with_pava(model, loss_fn, test_dataloaders, pava_dataloaders, metrics, params):
  """Evaluate the model on `num_steps` batches.

    Args:
        model: (torch.nn.Module) the neural network
        loss_fn: a function that takes batch_output and batch_labels and computes the loss for the batch
        test_dataloaders: dict of name to  (DataLoader)
          to evaluate metrics after running pava
        pava_dataloaders: dict of name to a DataLoader for regressing the scores/logit to probabilities
        metrics: (dict) a dictionary of functions that compute a metric using the output and labels of each batch
        params: (Params) hyperparameters
  """

  # set model to evaluation mode
  model.eval()

  all_metrics = {}
  all_datapoints = {}
  for pava_set_name, pava_dataloader in pava_dataloaders.iteritems():
    isoreg = run_isoreg(model, pava_dataloader, params, all_datapoints, pava_set_name)

    # loop through evaluation datasets
    evaluation_metrics = {}
    for set_name, dataloader in test_dataloaders.iteritems():
      run_evaluate(model, loss_fn, dataloader, params, isoreg, evaluation_metrics, metrics, set_name)
    all_metrics['pava_on_'+pava_set_name] = evaluation_metrics
  return all_metrics, all_datapoints

if __name__ == '__main__':
    """
        Evaluate the model on the test set.
    """
    # Load the parameters
    args = parser.parse_args()
    json_path = os.path.join(args.model_dir, 'params.json')
    assert os.path.isfile(json_path), "No json configuration file found at {}".format(json_path)
    params = utils.Params(json_path)

    # use GPU if available
    params.cuda = torch.cuda.is_available()     # use GPU is available

    # Set the random seed for reproducible experiments
    torch.manual_seed(1357)
    if params.cuda: torch.cuda.manual_seed(1357)

    # Get the logger
    utils.set_logger(os.path.join(args.model_dir, 'evaluate.log'))

    # Create the input data pipeline
    logging.info("Creating the dataset...")

    # fetch dataloaders
    test_dataloaders = data_loader.fetch_dataloaders(params.test_pava_on, args.data_dir, params)
    pava_dataloaders = data_loader.fetch_dataloaders(params.run_pava_on, args.data_dir, params)

    #test_dl = dataloaders['test']

    logging.info("- done.")

    # Define the model
    model = net.Net(params).cuda() if params.cuda else net.Net(params)

    loss_fn = net.loss_fn
    metrics = net.metrics

    logging.info("Starting evaluation")

    # Reload weights from the saved file
    utils.load_checkpoint(os.path.join(args.model_dir, args.restore_file + '.pth.tar'), model)

    # Evaluate
    test_metrics, all_datapoints = evaluate_with_pava(model, loss_fn,
        test_dataloaders, pava_dataloaders, metrics, params)
    save_path = os.path.join(args.model_dir, "metrics_pava_test_{}.json".format(args.restore_file))
    utils.save_dict_to_json(test_metrics, save_path)
    logging.info('saving data points')
    for tag, datapoints in all_datapoints.items():
      utils.save_datapoints(datapoints, args.model_dir, tag)
