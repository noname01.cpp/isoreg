"""Defines the neural network, losss function and metrics"""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    """
    This is the standard way to define your own network in PyTorch. You typically choose the components
    (e.g. LSTMs, linear layers etc.) of your network in the __init__ function. You then apply these layers
    on the input step-by-step in the forward function. You can use torch.nn.functional to apply functions

    such as F.relu, F.sigmoid, F.softmax, F.max_pool2d. Be careful to ensure your dimensions are correct after each
    step. You are encouraged to have a look at the network in pytorch/nlp/model/net.py to get a better sense of how
    you can go about defining your own network.

    The documentation for all the various components available o you is here: http://pytorch.org/docs/master/nn.html
    """

    def __init__(self, params):
        """
        We define an convolutional network that distinguishes dogs and cats in an image. The components
        required are:
        Args:
            params: (Params) contains num_channels, input_size, dropout_rate
        """
        super(Net, self).__init__()
        self.num_channels = params.num_channels
        self.input_size = params.input_size

        # each of the convolution layers below have the arguments (input_channels, output_channels, filter_size,
        # stride, padding). We also include batch normalisation layers that help stabilise training.
        # For more details on how to use these layers, check out the documentation.
        self.conv1 = nn.Conv2d(3, self.num_channels, 3, stride=1, padding=1)
        self.bn1 = nn.BatchNorm2d(self.num_channels)
        self.conv2 = nn.Conv2d(self.num_channels, self.num_channels*2, 3, stride=1, padding=1)
        self.bn2 = nn.BatchNorm2d(self.num_channels*2)
        self.conv3 = nn.Conv2d(self.num_channels*2, self.num_channels*4, 3, stride=1, padding=1)
        self.bn3 = nn.BatchNorm2d(self.num_channels*4)

        self.conv4 = nn.Conv2d(self.num_channels*4, self.num_channels*8, 3, stride=1, padding=1)
        self.bn4 = nn.BatchNorm2d(self.num_channels*8)

        self.AvgPool2d = nn.AvgPool2d(self.input_size//16)

        # 2 fully connected layers to transform the output of the convolution layers to the final output
        self.fc1 = nn.Linear(self.num_channels*8, self.num_channels*8)
        self.fcbn1 = nn.BatchNorm1d(self.num_channels*8)
        self.fc2 = nn.Linear(self.num_channels*8, 1)
        self.dropout_rate = params.dropout_rate

    def forward(self, s):
        """
        This function defines how we use the components of our network to operate on an input batch.

        Args:
            s: (Variable) contains a batch of images, of dimension batch_size x 3 x 64 x 64 .

        Returns:
            out: (Variable) dimension batch_size x 6 with the log probabilities for the labels of each image.

        Note: the dimensions after each step are provided
        """
        #                                                  -> batch_size x 3 x sz x sz
        # we apply the convolution layers, followed by batch normalisation, maxpool and relu x 3
        s = self.bn1(self.conv1(s))                         # batch_size x num_channels x sz x sz
        s = F.relu(F.max_pool2d(s, 2))                      # batch_size x num_channels x sz/2 x sz/2
        s = self.bn2(self.conv2(s))                         # batch_size x num_channels*2 x sz/2 x sz/2
        s = F.relu(F.max_pool2d(s, 2))                      # batch_size x num_channels*2 x sz/4 x sz/2
        s = self.bn3(self.conv3(s))                         # batch_size x num_channels*4 x sz/4 x sz/4
        s = F.relu(F.max_pool2d(s, 2))                      # batch_size x num_channels*4 x sz/8 x sz/8
        s = self.bn4(self.conv4(s))                         # batch_size x num_channels*8 x sz/8 x sz/8
        s = F.relu(F.max_pool2d(s, 2))                      # batch_size x num_channels*8 x sz/16 x sz/16

        s = self.AvgPool2d(s)                 # batch_size * num_channels*8
        # flatten the output for each image
        s = s.view(-1, self.num_channels*8)
        # apply 2 fully connected layers with dropout
        s = F.dropout(F.relu(self.fcbn1(self.fc1(s))),
            p=self.dropout_rate, training=self.training)    # batch_size x self.num_channels*8
        logits = self.fc2(s)                                     # batch_size x 1
        probs = torch.sigmoid(logits)
        # apply log softmax on each image's output (this is recommended over applying softmax
        # since it is numerically more stable)
        #return F.log_softmax(s, dim=1)
        return logits, probs


def loss_fn(preds, labels, is_logit=True):
    """
    Compute the cross entropy loss given outputs and labels.

    Args:
        outputs: (Variable) dimension batch_size x 1 - output of the model
        labels: (Variable) dimension batch_size, where each element is a value in [0, 1]

    Returns:
        loss (Variable): cross entropy loss for all images in the batch

    Note: you may use a standard loss function from http://pytorch.org/docs/master/nn.html#loss-functions. This example
          demonstrates how you can easily define a custom loss function.
    """
    if is_logit:
      loss =  nn.BCEWithLogitsLoss()
    else:
      loss = nn.BCELoss()
    return loss(preds, labels.reshape(preds.shape).float())

def accuracy(scores, labels, threshold=0.0):
    """
    Compute the accuracy, given the outputs and labels for all images.

    Args:
        scores: (np.ndarray) dimension batch_size x 1 -  output of the model (before/after sigmoid)
        labels: (np.ndarray) dimension batch_size, where each element is a value in [0, 1]

    Returns: (float) accuracy in [0,1]
    """
    is_target = (scores >= threshold).flatten() # logits >= 0 is equivalent to p > 0.5
    return np.sum(is_target==labels)/float(labels.size)

# maintain all metrics required in this dictionary- these are used in the training and evaluation loops
metrics = {
    'accuracy': accuracy,
    # could add more metrics such as accuracy for each token type
}
