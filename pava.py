import numpy as np
import scipy
from scipy import interpolate
from scipy import ndimage

def sigmoid(x):
    return 1/(1. + np.exp(-x))

def make_sorted(scores, labels):
    sorted_scores_indices = np.argsort(scores)
    labels = labels[sorted_scores_indices]
    scores = scores[sorted_scores_indices]
    #TODO:
    unique_scores, unique_indices = np.unique(scores, return_index=True)
    if len(scores) != len(unique_scores):
      print('Warning: scores with len {} are not unique with len {}'.format(len(scores), len(unique_scores)))
    #scores = unique_scores
    #labels = labels[unique_indices]
    return scores, labels

def make_data(ndata0=1000, ndata1=1000):
    total_data = ndata0 + ndata1
    proportion_1 = ndata1 / float(total_data)
    u_rands = np.random.rand(total_data)
    labels = (u_rands < proportion_1).astype(np.float32)
    grand_1 = 0.7 * np.random.randn(total_data) + 0.75
    grand_0 = 0.7 * np.random.randn(total_data) + 0.25
    scores = grand_1
    scores[ labels == 0] = grand_0[labels == 0]
    return make_sorted(scores, labels)

def bce_pava(a, threshold=0.5):
    n = a.shape[0]
    y = np.zeros((n,))
    # scores for different blocks
    block_a = np.zeros((n,))

    #number of samples for label 0 and 1 in each block
    wp0 = np.zeros((n,))
    wp1 = np.zeros((n,))

    # end of each block
    block_end = np.arange(n, dtype=np.int32) #np.zeros((n-1,), dtype=np.uint32)

    # j indexes the blocks. Within each block, all values
    # of y are equal
    j = -1
    for i in range(n):
        # Start a new block (provisionally) consisting of new data
        j += 1
        block_a[j] = a[i]
        wp0[j] = np.float32(a[i] < threshold)
        wp1[j] = 1 - wp0[j]
        block_end[j] = i

        # Search back, merging, until the values are in the right order
        while j>0 and block_a[j-1] > block_a[j]:
            # merging block j into block j-1. This is for the loss
            # y0 log(z) + (1-y0) log(1-z)
            wp0[j-1] += wp0[j]
            wp1[j-1] += wp1[j]
            block_a[j-1] = wp1[j-1] / (wp1[j-1] + wp0[j-1])
            block_end[j-1] = block_end[j]
            # end of merging block j-1 and j
            j -= 1
    k = 0
    for m in range(n):
        y[m] = block_a[k]
        if m == block_end[k]:
            k += 1
    return y

class BCEPava:
  def __init__(self, is_logit=False, clip=True,
                interp_kind='linear', smooth=True, sigma=50.0):
    self._threshold = 0.0 if is_logit else 0.5 #threshold for logit or prob
    self._clip = clip
    self._interp_kind = interp_kind
    self._smooth = smooth
    self._sigma = sigma

  def fit(self, scores, labels):
    assert scores.shape == labels.shape, '''scores shape {} is
        not equal to labels shape {}'''.format(scores.shape, labels.shape)
    scores, labels = make_sorted(scores, labels)
    self._scores = scores
    self._labels = labels

    self._y = bce_pava(labels, self._threshold)
    self._original_y = self._y
    if self._smooth:
      self._y = scipy.ndimage.gaussian_filter1d(self._y, self._sigma)
    self._f = interpolate.interp1d(scores, self._y, kind=self._interp_kind,
                            bounds_error=False)
    self._minX = np.min(scores)
    self._maxX = np.max(scores)

  def predict(self, X):
    X = np.clip(X, self._minX, self._maxX)
    return self._f(X)

  @property
  def scores(self):
    return self._scores

  @property
  def targets(self):
    return self._y

  @property
  def labels(self):
    return self._labels

if __name__ == '__main__':
    scores, labels = make_data(10000,10000)
    y = bce_pava(labels)
    from IPython import embed;embed()
