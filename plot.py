import matplotlib as mpl
mpl.use('pdf')
import matplotlib.pyplot as plt

import os, sys
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='experiments/base_model', help='Directory where the datapoints are saved')
parser.add_argument('--tag', default='val', help='TAG name for datapoints where pava was used')


if __name__ == '__main__':
  args = parser.parse_args()
  names = ['scores', 'labels', 'preds']
  data = {}
  for name in names:
    filepath = os.path.join(args.model_dir, args.tag + '_' + name + '.npy')
    data[name] = np.load(filepath)
  scores = data['scores']
  labels = data['labels']
  preds = data['preds']
  idx = np.argsort(scores)
  plt.plot(scores[idx], preds)
  savepath = os.path.join(args.model_dir, args.tag + '.pdf')
  plt.savefig(savepath)
