#!/bin/bash
export CUDA_VISIBLE_DEVICES=2
./plot.sh experiments/base_train/sigma/300
./plot.sh experiments/base_train/on_val
./plot.sh experiments/base_train/sigma/200
./plot.sh experiments/base_train/sigma/100
./plot.sh experiments/base_train/sigma/50
./plot.sh experiments/base_train/sigma/10
./plot.sh experiments/base_train/before_sigmoid/on_train
./plot.sh experiments/base_train/before_sigmoid/on_val
