#!/bin/bash
export CUDA_VISIBLE_DEVICES=2
python train_with_pava.py --model_dir=experiments/base_train/sigma/300
./plot.sh experiments/base_train/sigma/300
python train_with_pava.py --model_dir=experiments/base_train/on_val
./plot.sh experiments/base_train/on_val
python train.py --model_dir=experiments/base_train/without_pava
python train_with_pava.py --model_dir=experiments/base_train/sigma/200
./plot.sh experiments/base_train/sigma/200
python train_with_pava.py --model_dir=experiments/base_train/sigma/100
./plot.sh experiments/base_train/sigma/100
python train_with_pava.py --model_dir=experiments/base_train/sigma/50
./plot.sh experiments/base_train/sigma/50
python train_with_pava.py --model_dir=experiments/base_train/sigma/10
./plot.sh experiments/base_train/sigma/10
python train_with_pava.py --model_dir=experiments/base_train/before_sigmoid/on_train
./plot.sh experiments/base_train/before_sigmoid/on_train
python train_with_pava.py --model_dir=experiments/base_train/before_sigmoid/on_val
./plot.sh experiments/base_train/before_sigmoid/on_val
